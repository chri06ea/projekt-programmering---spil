﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kryds_og_bolle
{
    class Spil
    {
        public void Kør()
        {
            Spiller spiller;
            string valg;
            char brik;
            List<int> koordinatSæt;

            spiller = null;
            valg = "";
            brik = ' ';
            koordinatSæt = new List<int>();

            this.Nulstil();

            do
            {
                Console.WriteLine("Runde " + runde);

                this.Tegn();

                spiller = runde % 2 == 0 ? spillerKryds : spillerBolle;
                brik = runde % 2 == 0 ? 'X' : 'O';

                Console.WriteLine("spiller " + brik + " indtast valg (ex. \"0,1\")");

                while(!ErGyldigtValg((valg = spiller.FåValg())))
                    Console.WriteLine("Ugyldigt valg");

                try
                {
                    koordinatSæt.Clear();

                    foreach (var karakter in valg.Split(','))
                        koordinatSæt.Add(int.Parse(karakter));

                    if (!this.bræt.SætFelt(koordinatSæt[0], koordinatSæt[1], brik))
                        throw new SystemException("Kunne ikke sætte felt");
                }
                catch(Exception exception)
                {
                    Console.WriteLine("Fejl: " + exception.Message);
                    continue;
                }

                runde++;
            } while (this.FåVinder() == 0);


            this.Tegn();

            Console.WriteLine("Spiller " + this.FåVinder() + " vandt");
            Console.ReadKey();
        }

        public char FåVinder()
        {
            return this.bræt.FåVinder();
        }

        private void Nulstil()
        {
            this.bræt.Nulstil();
            runde = 0;
        }

        private void Tegn()
        {
            this.bræt.Tegn();
        }

        private bool ErGyldigtValg(string valg)
        {
            return valg.Length == 3 && char.IsDigit(valg[0]) && valg[1] == ',' && char.IsDigit(valg[2]);
        }

        private int runde = 0;
        private Bræt bræt = new Bræt();
        private Spiller spillerKryds = new Spiller();
        private Spiller spillerBolle = new Spiller();
    }
}
