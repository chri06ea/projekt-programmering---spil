﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kryds_og_bolle
{
    class Bræt
    {
        public void Nulstil()
        {
            brætFelter = new Brætfelt[brætStørrelse, brætStørrelse];

            for (var x = 0; x < brætStørrelse; x++)
                for (var y = 0; y < brætStørrelse; y++)
                    brætFelter[x, y] = new Brætfelt();
        }

        public void Tegn()
        {
            for (var x = 0; x < brætStørrelse; x++)
            {
                for (var y = 0; y < brætStørrelse; y++)
                    Console.Write("[" + brætFelter[x, y].fåVærdi() + "]");
                Console.Write("\n");
            }
        }

        public char FåVinder()
        {
            char[] feltVærdier;

            feltVærdier = new char[] { 'O', 'X' };

            //For O og X
            foreach (var feltVærdi in feltVærdier)
            {
                //Tjek vertikalt og horizontalt
                //retning 0 = vertikalt, retning 1 = horizontalt
                for(var retning = 0; retning < 2; retning++)
                {
                    //vertikalt el. horizontalt
                    for(var i = 0;  i < brætStørrelse; i++)
                    {
                        var antalFeltVærdierIRetning = 0;

                        //vertikalt el. horizontalt
                        for (var k = 0; k < brætStørrelse; k++)
                            if (this.brætFelter[(retning == 0 ? i : k), (retning == 0 ? k : i)].fåVærdi() == feltVærdi)
                                antalFeltVærdierIRetning++;

                        if (antalFeltVærdierIRetning == 3)
                            return feltVærdi;
                    }
                }
                //Tjek på kryds
                if(this.brætFelter[1, 1].fåVærdi() == feltVærdi)
                {
                    if (this.brætFelter[0, 0].fåVærdi() == feltVærdi && this.brætFelter[2, 2].fåVærdi() == feltVærdi)
                        return feltVærdi;

                    if (this.brætFelter[0, 2].fåVærdi() == feltVærdi && this.brætFelter[2, 0].fåVærdi() == feltVærdi)
                        return feltVærdi;
                }
            }

            return '\0';
        }

        public bool SætFelt(int x, int y, char karakter)
        {
            try
            {
                return brætFelter[x, y].SætFelt(karakter);
            }
            catch (Exception e)
            {
                return false;
            }
        }

        //Brættets størrelse (brættets alreal brætStørelse^2)
        const int brætStørrelse = 3;
        private Brætfelt[,] brætFelter = new Brætfelt[brætStørrelse, brætStørrelse];
    }
}
