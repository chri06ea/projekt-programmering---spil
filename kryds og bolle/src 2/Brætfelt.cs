﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kryds_og_bolle
{
    class Brætfelt
    {
        public bool SætFelt(char feltVærdi)
        {
            //Ugyldig felt værdi check
            if (feltVærdi != 'O' && feltVærdi != 'X')
                return false;

            //Felt er taget
            if (feltVærdi_ != ' ')
                return false;

            feltVærdi_ = feltVærdi;

            return true;
        }

        public char fåVærdi()
        {
            return feltVærdi_;
        }

        private char feltVærdi_ = ' ';
    }
}
