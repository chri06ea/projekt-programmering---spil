﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kryds_og_bolle
{
    class Program
    {
        static void Main(string[] args)
        {
            string spillerValg;
            int brikkerSat = 0;
            int runde = 0;
            char brik;
            int spillerValg1, spillerValg2;
            char[,] bræt = new char[3, 3];
            do
            {
                Console.WriteLine("(" + bræt[0, 0] + ")(" + bræt[0, 1] + ")(" + bræt[0, 2] + ")\n(" + bræt[1, 0] + ")(" + bræt[1, 1] + ")(" + bræt[1, 2] + ")\n(" + bræt[2, 0] + ")(" + bræt[2, 1] + ")(" + bræt[2, 2] + ")");
                Console.WriteLine("Indtast koordinat for brik");

                if (runde % 2 == 0)
                    brik = 'X';
                else
                    brik = 'O';

                spillerValg = Console.ReadLine();

                try
                {
                    spillerValg1 = int.Parse("" + spillerValg[0]);
                    spillerValg2 = int.Parse("" + spillerValg[2]);
                }
                catch (Exception e)
                {
                    Console.WriteLine("ugyldigt input");
                    continue;
                }
                if (spillerValg1 >= 3 || spillerValg2 >= 3)
                {
                    Console.WriteLine("Ugyldigt input");
                    continue;
                }
                if (bræt[spillerValg1, spillerValg2] != 0)
                {
                    Console.WriteLine("Der er´allerede placeret en brik her!");
                    continue;
                }

                bræt[spillerValg1, spillerValg2] = brik;
                if (bræt[0, 0] == brik && bræt[0, 1] == brik && bræt[0, 2] == brik)
                {
                    Console.WriteLine("tillykke spiller " + brik + ". Du har vundet.");
                    Console.ReadLine();
                    break;
                }
                if (bræt[1, 0] == brik && bræt[1, 1] == brik && bræt[1, 2] == brik)
                {
                    Console.WriteLine("tillykke spiller " + brik + ". Du har vundet.");
                    Console.ReadLine();
                    break;
                }
                if (bræt[2, 0] == brik && bræt[2, 1] == brik && bræt[2, 2] == brik)
                {
                    Console.WriteLine("tillykke spiller " + brik + ". Du har vundet.");
                    Console.ReadLine();
                    break;
                }
                if (bræt[0, 0] == brik && bræt[1, 0] == brik && bræt[2, 0] == brik)
                {
                    Console.WriteLine("tillykke spiller " + brik + ". Du har vundet.");
                    Console.ReadLine();
                    break;
                }
                if (bræt[0, 1] == brik && bræt[1, 1] == brik && bræt[2, 1] == brik)
                {
                    Console.WriteLine("tillykke spiller " + brik + ". Du har vundet.");
                    Console.ReadLine();
                    break;
                }
                if (bræt[0, 2] == brik && bræt[1, 2] == brik && bræt[2, 2] == brik)
                {
                    Console.WriteLine("tillykke spiller " + brik + ". Du har vundet.");
                    Console.ReadLine();
                    break;
                }
                if (bræt[0, 0] == brik && bræt[1, 1] == brik && bræt[2, 2] == brik)
                {
                    Console.WriteLine("tillykke spiller " + brik + ". Du har vundet.");
                    Console.ReadLine();
                    break;
                }
                if (bræt[0, 2] == brik && bræt[1, 1] == brik && bræt[2, 0] == brik)
                {
                    Console.WriteLine("tillykke spiller " + brik + ". Du har vundet.");
                    Console.ReadLine();
                    break;
                }

                brikkerSat++;
                runde++;


            }
            while (brikkerSat != 9);
        }
    }
}
