﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace krig
{
    public enum CardValue : int { ACE, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, MAX };
    public enum CardType { DIAMOND, SPADES, HEART, CLOVER, MAX }
    public class Card
    {
        public Card(CardType cardType_, CardValue cardValue_)
        {
            this.cardType = cardType_;
            this.cardValue = cardValue_;
        }

        private CardType cardType;
        private CardValue cardValue;
    }

    public class Deck
    {
        public Deck()
        {
            ResetDeck();
        }

        public void ResetDeck()
        {
            Fill();
            Shuffle();
        }

        public Card TrækCard()
        {
            Card card = cards[0];
            cards.RemoveAt(0);
            return card;
        }

        private void Fill()
        {
            cards.Clear();

            for (var cardValue = CardValue.ACE; cardValue < CardValue.MAX; cardValue++)
            {
                for (var cardType = CardType.DIAMOND; cardType < CardType.MAX; cardType++)
                {
                    cards.Add(new Card(cardType, cardValue));
                }
            }
        }



        public void Shuffle()
        {
            List<int> k = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            Random rnd = new Random();
            cards = cards.OrderBy(x => rnd.Next()).ToList();
        }

        private List<Card> cards = new List<Card>();
    }

    public class Spiller
    {
        public Spiller(string spillerNavn_)
        {
            spillerNavn = spillerNavn_;
        }

        private string spillerNavn;
    }

    class Krig
    {
        public Krig()
        {

        }

        public void Spil()
        {

        }

        public void TilføjSpiller(string spillerNavn)
        {
            spillere.Add(new Spiller(spillerNavn));
        }

        private Deck deck = new Deck();
        private List<Spiller> spillere = new List<Spiller>();
    }

    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
